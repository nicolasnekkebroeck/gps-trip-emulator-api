package TripgpsdatasimulatorAPI

import (
	"net/http"

	"bitbucket.org/be-mobile/go-lib/http-server/router"
)

// getRoutes holds the http_server routes definition
func (api *TripgpsdatasimulatorAPI) getRoutes() []router.Route {
	return []router.Route{
		{
			Method:      http.MethodGet,
			Path:        "/info",
			Description: "Hello back to ?name=",
			Protected:   false,
			Protection:  []string{"scope"},
			Log:         true,
			HandlerFunc: api.info,
		},
		{
			Method:      http.MethodGet,
			Path:        "/fakeRoute",
			Description: "Get the route zith the noice points",
			Protected:   false,
			Protection:  []string{"scope"},
			Log:         true,
			HandlerFunc: api.coordinates,
		},
		{
			Method:      http.MethodGet,
			Path:        "/visualize",
			Description: "Draw the new route",
			Protected:   false,
			Protection:  []string{"scope"},
			Log:         true,
			HandlerFunc: api.draw,
		},
		{
			Method:      http.MethodGet,
			Path:        "/reevaluate",
			Description: "When from, to or radius has been changed, this is to be called in order to recalculate the route and noice",
			Protected:   false,
			Protection:  []string{"scope"},
			Log:         true,
			HandlerFunc: api.reevaluate,
		},
		{
			Method:      http.MethodPost,
			Path:        "/addJSONToBody",
			Description: "add JSON params",
			Protected:   false,
			Protection:  []string{"scope"},
			Log:         true,
			HandlerFunc: api.coordinatesWithJSON,
		},
	}
}
