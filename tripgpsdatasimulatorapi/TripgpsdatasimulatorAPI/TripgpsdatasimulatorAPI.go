package TripgpsdatasimulatorAPI

import (
	"fmt"

	"net/http"

	"bitbucket.org/be-mobile/go-lib/http-server/router"
	"bitbucket.org/be-mobile/go-lib/sirupsen/logrus"
	mux "github.com/gorilla/mux"
)

// API is a type to serve http content according to a config
type TripgpsdatasimulatorAPI struct {
	router *mux.Router
	server *http.Server
}

// InitNewAPI loads configurations and inits the API accordingly
func InitNewAPI() (*TripgpsdatasimulatorAPI, error) {
	// parse loglevel
	logLevel, err := logrus.ParseLevel("info")
	if err != nil {
		return nil, err
	}
	logrus.SetLevel(logLevel)

	// create new API
	api := &TripgpsdatasimulatorAPI{}

	// create router
	router, err := router.New(api.getRoutes(), nil, api.optionsHandler)
	if err != nil {
		return nil, err
	}

	api.router = router

	api.server = &http.Server{}
	api.server.Addr = fmt.Sprintf("%v", ":8080")
	api.server.Handler = api.router
	// return our API!
	return api, nil
}

// Start the http_server ListenAndServe method for our API.
// This call is blocking.
func (api *TripgpsdatasimulatorAPI) Start() error {
	//api.server.ListenAndServe(":8080", nil)
	return api.server.ListenAndServe()
}

// Stop shuts down the API
func (api *TripgpsdatasimulatorAPI) Stop() error {
	return api.server.Shutdown(nil)
}
