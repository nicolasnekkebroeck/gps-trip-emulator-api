package TripgpsdatasimulatorAPI

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/be-mobile/go-lib/sirupsen/logrus"
	geo "github.com/kellydunn/golang-geo"
	geojson "github.com/paulmach/go.geojson"
	log "github.com/sirupsen/logrus"
)

const url = "http://hiplanner.be-mobile-ops.net/map/DE_20180513_ETC_1/route?calcPoints=true&include=segments&instructions=false&type=json&point=%f,%f&point=%f,%f"

//const outputFile = "../Data/output.json"

var randomDistance float64
var coordinates Coordinates
var timeBetweenPoints = 10.00
var inputs []InputFormatAxes

// Point contains Array of GPS coordinates
type Point struct {
	Coordinates [][]float64 `json:"coordinates"`
}

// Leg contains the route informmation
type Leg struct {
	Distance float64 `json:"distance"`
	Duration float64 `json:"duration"`
	Mode     string  `json:"mode"`
	Segments []int64 `json:"segments"`
	Type     string  `json:"type"`
}

// Route holds the legs and the GPS coordinates
type Route struct {
	Duration float64 `json:"duration"`
	Distance float64 `json:"distance"`
	Legs     []Leg   `json:"legs"`
	Points   Point   `json:"points"`
}

// Info contains additional info regarding the HighPlanner route object
type Info struct {
	CalculationTime        float64   `json:"calculationTime"`
	DistanceToNetworkPoint []float64 `json:"distancePointsToNetwork"`
	VisualizationTime      int64     `json:"visualisationTime"`
}

// FeatureCollection JSON representation of the HighPlanner response
type FeatureCollection struct {
	Routes []Route `json:"routes"`
	Info   Info    `json:"info"`
}

// InputFormatAxes is the struct type saved by the API
type InputFormatAxes struct {
	ProviderID                   int
	OBUID                        string
	TimeStamp                    time.Time
	OriginalLatitude             float64
	OriginalLongitude            float64
	BluredLatitude               float64
	BluredLongitude              float64
	GpsSpeed                     float64
	GpsBear                      int
	HDOP                         int
	Ptac                         int
	Axles                        int
	EuroClass                    int
	CountryCode                  int
	LicensePlate                 string
	TruckHeight                  int
	TruckLength                  int
	TruckWidth                   int
	PAN                          string
	PositionIncrementalID        int
	OBUInException               int
	VehicleCurrentMaxTrainWeight int
	TollCategory                 string
	NBAxlesTrailer               int
}

type InputsFromAxes struct {
	Inputs []InputFormatAxes
}

type InputCoordinate struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type Input struct {
	From         InputCoordinate `json:"from"`
	To           InputCoordinate `json:"to"`
	RandomRadius float64         `json:"randomRadius"`
	Speed        float64         `json:"speed"`
}

type Coordinates struct {
	FromLatitude  float64
	FromLongitude float64
	ToLatitude    float64
	ToLongitude   float64
}

// optionsHandler to satisfy preflight requests
func (api *TripgpsdatasimulatorAPI) optionsHandler(writer http.ResponseWriter, req *http.Request) {
	writer.Header().Set("Allow", "GET,POST")
}

// index returns the api name and supported calls
func (api *TripgpsdatasimulatorAPI) index(w http.ResponseWriter, r *http.Request) {
	routesString := ""
	for _, route := range api.getRoutes() {
		routesString += fmt.Sprintf("\n\t%v\t%v\t%v", route.Method, route.Path, route.Description)
	}

	// Set the content-type header
	w.Header().Set("content-type", "text/plain")

	// Write our message
	code, err := w.Write([]byte(fmt.Sprintf("API\n%v", routesString)))
	if err != nil {
		logrus.Errorf("Can't deliver index() response: %v (status %v)", err, code)
	}
}

// hello fetches the name from the ?name variable, and
// returns with a message "Hello <name>".
func (api *TripgpsdatasimulatorAPI) info(w http.ResponseWriter, r *http.Request) {
	// Set the content-type header
	w.Header().Set("content-type", "text/plain")

	// Write our message
	fmt.Fprintf(w, "Hello, \n This api is meant to compute a route from to a point and to add noici to some points. It also generates a JSON File with the resulting points.")
}

func (api *TripgpsdatasimulatorAPI) coordinatesWithJSON(w http.ResponseWriter, r *http.Request) {
	logrus.Print("coordinatesWithJSON called")
	coordinates = Coordinates{}
	var u Input
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	coordinates.FromLatitude = u.From.Latitude
	coordinates.FromLongitude = u.From.Longitude
	coordinates.ToLatitude = u.To.Latitude
	coordinates.ToLongitude = u.To.Longitude
	randomDistance = u.RandomRadius
	timeBetweenPoints = u.Speed
	getRouteAndAddNoice(coordinates)
	json.NewEncoder(w).Encode(inputs)
}

func (api *TripgpsdatasimulatorAPI) coordinates(w http.ResponseWriter, r *http.Request) {
	coordinates = Coordinates{}
	vars := r.URL.Query()
	key := make([]string, 4)
	value, err := strconv.ParseFloat(vars.Get("randomDistance"), 64)
	if err != nil {
		check(err)
		randomDistance = rand.Float64()
	} else {
		randomDistance = value
	}
	keys := vars.Get("coordinates")
	key = strings.Split(keys, ",")
	coordinates.FromLatitude, _ = strconv.ParseFloat(key[0], 64)
	coordinates.FromLongitude, _ = strconv.ParseFloat(key[1], 64)
	coordinates.ToLatitude, _ = strconv.ParseFloat(key[2], 64)
	coordinates.ToLongitude, _ = strconv.ParseFloat(key[3], 64)
	getRouteAndAddNoice(coordinates)
	json.NewEncoder(w).Encode(inputs)
	fmt.Fprintf(w, "Json file: %v", inputs)
}

func (api *TripgpsdatasimulatorAPI) reevaluate(w http.ResponseWriter, r *http.Request) {
	coordinates = Coordinates{}
	getRouteAndAddNoice(coordinates)
	json.NewEncoder(w).Encode(inputs)
}

// Does the whole thing with 2 GPS coordinates passed: gets the route, add noice effect and generates the visuals for the GEOCode
func getRouteAndAddNoice(coordinates Coordinates) {
	rand.Seed(5)
	referenceTime := time.Now()
	route := getHighPlannerRoute(coordinates.FromLatitude, coordinates.FromLongitude, coordinates.ToLatitude, coordinates.ToLongitude)
	routes := route.Routes
	coordinatesOfRoute := routes[0].Points
	for i := 0; i < len(coordinatesOfRoute.Coordinates); i++ {
		currentPoint := coordinatesOfRoute.Coordinates[i]
		currentLong := currentPoint[0]
		currentLat := currentPoint[1]

		// Create Dummy input From Axes with these long and lat
		if referenceTime.Sub(time.Now()).Seconds() < timeBetweenPoints {
			var currentInput InputFormatAxes
			currentInput.OriginalLatitude = currentLat
			currentInput.OriginalLongitude = currentLong
			currentInput.ProviderID = 130
			referenceTime = referenceTime.Add(time.Second * time.Duration(timeBetweenPoints))
			currentInput.TimeStamp = referenceTime
			currentInput.OBUID = "374729985"
			currentInput.BluredLatitude, currentInput.BluredLongitude = getLocation(currentInput.OriginalLatitude, currentInput.OriginalLongitude, randomDistance)
			inputs = append(inputs, currentInput)
		} else {
			referenceTime = time.Now()
		}
	}
}

func addSpeedBetweenPoints(inputs []InputFormatAxes) {
	for i := 0; i <= len(inputs)-2; i++ {
		j := i + 1
		currentElement := inputs[i]
		nextElement := inputs[j]
		distance := distanceInMBetweenEarthCoordinates(currentElement.OriginalLatitude, currentElement.OriginalLongitude, nextElement.OriginalLatitude, nextElement.OriginalLongitude)
		speedInKMPerSecond := distance
		inputs[i].GpsSpeed = speedInKMPerSecond
	}
}

func degreesToRadians(degrees float64) float64 {
	return degrees * math.Pi / 180.00
}

func distanceInMBetweenEarthCoordinates(lat1, lon1, lat2, lon2 float64) float64 {
	var earthRadius = 6371000.00

	var dLat = degreesToRadians(lat2 - lat1)
	var dLon = degreesToRadians(lon2 - lon1)

	lat1 = degreesToRadians(lat1)
	lat2 = degreesToRadians(lat2)

	var a = math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Sin(dLon/2)*math.Sin(dLon/2)*math.Cos(lat1)*math.Cos(lat2)
	var c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return earthRadius * c
}

// Will return the lat and long coordinates of a point within a random radius(!in meters!)
func getLocation(lat0 float64, long0 float64, radius float64) (latitude float64, longitude float64) {
	t := 2.0 * math.Pi * rand.Float64() //  random angle in rad
	latdelta := math.Cos(t)
	longdelta := math.Sin(t)

	circledistancem := 1000.0 * geo.NewPoint(lat0, long0).GreatCircleDistance(geo.NewPoint(lat0+latdelta, long0+longdelta))

	desireddistancem := radius * rand.Float64()
	scalefactor := desireddistancem / circledistancem

	return lat0 + latdelta*scalefactor, long0 + longdelta*scalefactor
}

// Will return highPlanner route with the different GPS points of that route
func getHighPlannerRoute(fromLattitude, fromLongitude, toLattitude, toLongitude float64) (route FeatureCollection) {
	urlHighplanner := fmt.Sprintf(url, fromLattitude, fromLongitude, toLattitude, toLongitude)
	res, err := http.Get(urlHighplanner)

	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		panic(err.Error())
	}

	var data FeatureCollection
	json.Unmarshal(body, &data)
	return data
}

func toOutputFile(elements []InputFormatAxes, fileName string) {
	file, err := os.Create(fileName)

	check(err)

	defer file.Close()

	for i := 0; i < len(elements); i++ {
		file.WriteString(elements[i].String())
	}

	file.Sync()
}

// Vizualize will generate the JSON for GEOcode visualisation
func Vizualize() []byte {
	fc := geojson.NewFeatureCollection()
	for i := 0; i < len(inputs); i++ {
		p1 := []float64{inputs[i].OriginalLongitude, inputs[i].OriginalLatitude}
		line := [][]float64{p1, []float64{inputs[i].BluredLongitude, inputs[i].BluredLatitude}}
		fc.AddFeature(geojson.NewLineStringFeature(line))
	}
	marshalled, error := json.Marshal(fc)
	if error != nil {
		check(error)
	}
	fmt.Println(string(marshalled))
	return marshalled
}

func (api *TripgpsdatasimulatorAPI) draw(w http.ResponseWriter, r *http.Request) {
	marshalled := Vizualize()
	json.NewEncoder(w).Encode(string(marshalled))
}

// WriteError writes an error to the given ResponseWriter
func WriteError(w http.ResponseWriter, code int, message string, args ...interface{}) {
	logrus.Errorf(message, args...)
	w.Header().Set("content-type", "application/json")
	http.Error(w, fmt.Sprintf(message, args...), code)
}

func check(err error) bool {
	if err != nil {
		log.Error(err)
		return false
	}
	return true
}

func checkFatal(err error) bool {
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
	return true
}

func (i InputFormatAxes) String() string {
	return fmt.Sprintf("%d;%s;%s;%f;%f;%f;%f,%f,%f,%d,%d,%d,%d,%d,%d,%d,%s,%d,%d,%d,%s,%d,%d,%d,%s,%d\n", i.ProviderID, i.OBUID, i.TimeStamp.Format(time.RFC3339), i.OriginalLatitude, i.OriginalLongitude, i.BluredLatitude, i.BluredLongitude, i.GpsSpeed, i.GpsBear, i.HDOP, i.Ptac, i.Axles, i.EuroClass, i.CountryCode, i.LicensePlate, i.TruckHeight, i.TruckLength, i.TruckWidth, i.PAN, i.PositionIncrementalID, i.OBUInException, i.VehicleCurrentMaxTrainWeight, i.TollCategory, i.NBAxlesTrailer)
}
