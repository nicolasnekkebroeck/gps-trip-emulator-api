package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/be-mobile/HIPlanner-Coordinates-Fuzzer/tripgpsdatasimulatorapi/TripgpsdatasimulatorAPI"
	"bitbucket.org/be-mobile/go-lib/sirupsen/logrus"
)

var version string

// Function only there for debugging purpose, soon to be deleted
func main() {
	logrus.Infof("API - Version %v", version)

	// Init loads config and inits new API
	api, err := TripgpsdatasimulatorAPI.InitNewAPI()
	if err != nil {
		logrus.Fatal(err)
	}
	// Graceful shutdown
	gracefulStop := make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM) // Kubernetes shutdown code
	signal.Notify(gracefulStop, syscall.SIGINT)  // CTRL + C

	go func() {
		<-gracefulStop

		fmt.Println("Received a quit signal. Stopping background work now..")

		if err := api.Stop(); err != nil {
			logrus.Errorf("Failed stopping API: %v", err)
		}
	}()

	// Start (blocking)
	if err := api.Start(); err != nil {
		logrus.Fatal(err)
	}

}
