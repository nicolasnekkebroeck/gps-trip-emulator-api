Noice adder to a Route
========================

# Introduction
This API computes a route based on a from and a to coordinate. These coordinates are made of a start lattitude and longitude and an end latitude and longitude.These parameters need to be floating points. 
These points are passed to HighPlanner and a route will be calculated and the route with GPS coordinates returned in the form of a JSON object. Based on these GPS coordinates a blur effect can now be added to the route. A JSON file is being created with the route to which the noice was added.


# /fakeRoute
@ parameters:
<ul>
    <li> coordinates: latitude and longitude from and to a point in floating point</li>
    <li> randomDistance: random size of the diameter in which the noicie should occur. This is to be given in meters in floating point format</li>
</ul>
eg:  GET /fakeRoute?coordinates=51.479256,9.148072,51.517711,8.690962&randomDistance=2.5<br />
Output: the result is written to the body of the response

``` curl
curl -X GET \
  'http://localhost:8080/fakeRoute?coordinates=51.479256,9.148072,51.517711,8.690962&randomDistance=2.5' \
  -H 'Postman-Token: 31dc85fb-864b-4d72-a80c-d8770c0897d4' \
  -H 'cache-control: no-cache'
```

# /info (This post delivers info about the API)
@ parameter
<ul>
</ul>
eg: GET /info will deliver a message with api info

# /visualize (Geocode intrepretation of the route after having called /fakeRoute)
@ parameter
<ul>
</ul>
eg: GET /visualize<br />
Output: the result is written to the body of the response

``` curl
curl -X GET \
  http://localhost:8080/visualize \
  -H 'Postman-Token: ede129a2-bdd1-4de3-a208-18ba964fa146' \
  -H 'cache-control: no-cache'
```

# /addJSONToBody (Add attributes to the api)
eg: POST /addJSONToBody <br />
Output: No outpout is given here, it is a HTTP Post, the content of the JSON file is added to the body of the request. <br />

``` curl
curl -X POST \
  http://localhost:8080/addJSONToBody \
  -H 'Postman-Token: 213e3ac9-0005-4c55-a7fd-b5cb56bfef91' \
  -H 'cache-control: no-cache' \
  -d '{  
    "from":{  
       "latitude":51.479256,
       "longitude":9.148072
    },
    "to":{  
       "latitude":51.517711,
       "longitude":8.690962
    },
    "RandomRadius":200.5,
    "Speed":120.50
 }'
```


# Data/input.json file
The purpose is to pass this json file to the api and let the api handle it as part of the request body
In this struct, we pass the from and to point, the randomRadius (distance to a certain point) and the speed
<h2>format:</h2>

``` json

{  
    "from":{  "latitude":51.479256, "longitude":9.148072},
    "to":{ "latitude":51.517711, "longitude":8.690962},
    "RandomRadius":2.5,
    "Speed":120.50
 }

 ``` 